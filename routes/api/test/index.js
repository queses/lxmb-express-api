"use strict"

var express = require('express')
var router = express.Router()
const fs = require('fs')
var path = require('path')

router.get('/test', function(req, res, next) {
  res.send('Hello')
})

router.get('/flat', function(req, res, next) {
  const PUBLIC_DIR = require(__basedir + './conf/patches').PUBLIC_DIR
  const IMG_URL = '/images/test/object/flat/'
  const IMG_FOLDER = PUBLIC_DIR + IMG_URL
  fs.readdir(IMG_FOLDER, (err, files) => {
    if (err) {
      console.log(err)
    } else {
      var images = files.map(file => {
        var url = getReqHost(req) + IMG_URL + file
        return url
      });
      res.send(JSON.stringify(images))
    }
  });
});

module.exports = router
