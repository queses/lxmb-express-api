"use strict"

var p = require('path');

module.exports.PUBLIC_DIR = p.resolve('./public');
module.exports.IMG_DIR = p.resolve('./public/images');

module.exports.IMG_URL = '/images/';
